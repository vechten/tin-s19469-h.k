var Auto = /** @class */ (function () {
    function Auto() {
    }
    Object.defineProperty(Auto.prototype, "rok", {
        get: function () {
            return this._rok;
        },
        set: function (value) {
            this._rok = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auto.prototype, "przebieg", {
        get: function () {
            return this._przebieg;
        },
        set: function (value) {
            this._przebieg = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auto.prototype, "cena_wyjsciowa", {
        get: function () {
            return this._cena_wyjsciowa;
        },
        set: function (value) {
            this._cena_wyjsciowa = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auto.prototype, "cena_koncowa", {
        get: function () {
            return this._cena_koncowa;
        },
        set: function (value) {
            this._cena_koncowa = value;
        },
        enumerable: true,
        configurable: true
    });
    Auto.prototype.powiekszCene = function () {
        this._cena_wyjsciowa = +1000;
    };
    Auto.prototype.pomniejszCene = function () {
        this._cena_wyjsciowa = this._cena_wyjsciowa - (2019 - this._rok) * 1000;
    };
    Auto.prototype.pomniejszCeneOPrzebieg = function () {
        this._cena_wyjsciowa = this._cena_wyjsciowa - (this._przebieg - this._przebieg % 100000) / 100000 * 100000;
    };
    Auto.prototype.setCenaWyjsciowa = function (cena, rok) {
        this._cena_wyjsciowa = cena;
        this._rok = rok;
        this.pomniejszCene();
        this.pomniejszCeneOPrzebieg();
    };
    return Auto;
}());
function addToArray(array, auto) {
    if (auto.cena_wyjsciowa > 10000) {
        array.push(auto);
    }
}
function makeThemOlder(array) {
    array.forEach(function (auto) { return auto.rok - 1; });
}
function pitagoras(a, b, c) {
    return ((a * a + b * b === c * c) || (a * a + c * c === b * b) || (b * b + c * c === a * a));
}
function podzielne(a, b, c) {
    for (let i = a; i === b; i++) {
        if (i % c === 0) {
            console.log(i);
        }
    }
}
function tabliczka(a) {
    for (let i = 1; i <= a; i++) {
        let linia = "";
        for (let j = 1; j <= a; j++) {
            linia += i * j + " ";
        }
        console.log(linia);
    }
}

function choinkaWDzien(x) {
    for (let i = 1; i <= x; i++){
        let linia = "";
        for(let j = 1; j <= i; j++){
            linia += '*'
        }
        console.log(linia);
    }
    
}
